

pragma solidity ^0.6.0;
import "./NeoToken.sol";
import "./INRToken.sol";
// import "./IINROracle.sol";
// import "./INEOOracle.sol";

// ----------------------------------------------------------------------------
// Implemetation of Automated Market Maker smart contract
// (c) by Charmee Gadoya .
//compile version is 0.6.0
// ----------------------------------------------------------------------------

contract AMMSmartContract{
    //uint public  etherToINRPrice; //used for storing the dynamic price of the ethertoinr conversion from the INROracle.
    //uint public  etherToNEOPrice;//used for storing the dynamic price of the ethertoineo conversion from the NEOOracle.
    
    uint token1Balance; // used for storing neo token1 balance.
    uint token2Balance; //used for storing inr token2 balance.
    uint priceOf1Token; // used for storing price of 1neo  token  dynamically calculated by conversion function .
    uint priceOf2Token; // used for storing price of 1neo  token  dynamically calculated by conversion function .
    uint totalInrTokens; // used for storing total inr tokens transfer to the user after dynamic calculated the amount by the conversion function.
    uint totalNeoTokens; // used for storing total neo tokens transfer to the user after dynamic calculated the amount by the conversion function.
    uint temporaryNewAsset1;//used for storing temporary value for   old asset 1 token value for the calculation of the new price and new asset token value for new conversion of specific tokens purpose;
    uint temporaryNewAsset2;//used for storing temporary value for   old asset 2 token value for the calculation of the new price and new asset token value for new conversion of specific tokens purpose;
    uint newAsset1;//used for storing temporary asset 1 value  for the dynamic calculation  for new conversion of specific tokens purpose;
    uint newAsset2;//used for storing temporary asset 2 value  for the dynamic calculation  for new conversion of specific tokens purpose;
    uint contractBalanceForAsset1; //used for the  storing contract balance for asset 1
    uint contractBalanceForAsset2; //used for the  storing contract balance for asset 2 

    uint8  token1Decimals; //used for storing decimals count for neo tokens;
    uint8  token2Decimals;//used for storing decimals count for inr tokens;

    string tokenName1;//used for storing token name for neo tokens;
    string tokenName2;//used for storing token name for inr tokens;

 
    //used for storing liquidity pool
    struct LiquidityPool {
        uint oldAsset1;
        uint oldAsset2;
        uint newAsset1;
        uint newAsset2;
        uint priceOf1Token;
        uint priceOf2Token;
        uint pool;
    }
     
    address payable owner; //used for transfering ether or tokens to the owner

    mapping(address => LiquidityPool) public liquiditypool; //used for storing tokens in asset 1,2 and liquidity pool variables.
    
    event createdLiquidity(address indexed owner, uint  asset1, uint  asset2, uint256 liqudityTokens); //event used for create liqudity function.
    event convertNeoToInrtokens(string message); //used for transmitting message for conversion of  neo to inr tokens 
    event convertInrToNeotokens(string message); //used for transmitting message for conversion of inr to neo tokens 

    IERC20 public token1;
    IERC20 public token2;
    // INEOOracle public neooracle;
    // IINROracle public inroracle;

    constructor(address neoToken,address inrToken,address payable ownerAddress) public {
        owner=ownerAddress;
        token1 = IERC20(neoToken); //token 1 in these smart contract represnet neo token
        token2 = IERC20(inrToken); //token 2 in these smart contract represnet inr token
        token1Decimals=token1.getDecimals();//used for  storing neo token decimal count
        token2Decimals=token2.getDecimals();//used for  storing inr token decimal count
        tokenName1=token1.getTokenname();//used for  storing neo token name 
        tokenName2=token2.getTokenname();//used for  storing inr token name 
        require(token1Decimals==18 ,"token is not neo token");
        require(keccak256(abi.encodePacked(tokenName1)) == keccak256(abi.encodePacked("NEO")),"token  is not neo token");
        require(token2Decimals==18 ,"token is not inr token");
        require(keccak256(abi.encodePacked(tokenName2)) == keccak256(abi.encodePacked("INR")),"token is not inr token");
        // neooracle = INEOOracle(_neooracle);
        // inroracle = IINROracle(_inroracle);
        // etherToINRPrice =inroracle.getPrice();
        // etherToNEOPrice =neooracle.getPrice();
        // address _neooracle,address _inroracle,
    }

    modifier onlyOwner() {
        require(isOwner());
        _;
    }

    /**
     * @return true if `msg.sender` is the owner of the contract.
     */
    function isOwner() public view returns (bool) {
        return msg.sender == owner;
    }

     /* used for creating liquidity pool */
    function createLiquidity(uint neoTokens , uint inrTokens)  public onlyOwner{
        require(neoTokens>999 ,"token ne should be greater than 999");
        require(inrTokens>999 ,"token inr should be greater than 999");
        require(neoTokens==inrTokens,"both token should be the same amount for adding proper liquidity");
         //checkes the balance of neo token of the owner .
        token1Balance=token1.balanceOf(msg.sender);
         //checkes the balance of inr token of the owner.
        token2Balance=token2.balanceOf(msg.sender);
        require(token1Balance > 0,"new token balance should  be greateer than 0"); 
        require(token2Balance > 0,"inr token balance should  be greater than 0");
        // require(liquiditypool[address(this)].pool<=0,"liqudidity pool is zero");
        require(liquiditypool[address(this)].newAsset1<=0,"neo token  should be  zero ");
        require(liquiditypool[address(this)].newAsset2<=0,"inr token should be zero ");
        contractBalanceForAsset1=token1.balanceOf(address(this));
        contractBalanceForAsset2=token1.balanceOf(address(this));
        require(contractBalanceForAsset1<=0,"contract balance for neo token should be zero ");
        require(contractBalanceForAsset2<=0,"contract balance for inr token should be zero ");
        //transfers owner neo tokens to these contract.
        token1.transferFrom(owner,address(this),neoTokens);
        //transfers owner inr tokens to these contract.
        token2.transferFrom(owner,address(this),inrTokens);
        //stores the speicific tokens in these contract
        liquiditypool[address(this)].oldAsset1 = neoTokens; 
        liquiditypool[address(this)].oldAsset2 = inrTokens; 
        liquiditypool[address(this)].newAsset1 = neoTokens; 
        liquiditypool[address(this)].newAsset2 = inrTokens; 
        liquiditypool[address(this)].priceOf1Token = 0; 
        liquiditypool[address(this)].priceOf2Token = 0;
        liquiditypool[address(this)].pool = neoTokens * inrTokens;
        emit createdLiquidity(msg.sender,neoTokens,inrTokens,neoTokens * inrTokens);
    }

     /*used for converting inr to neo tokens*/
    function convertNEOTOINR(uint neoTokens)  public {
        require(msg.sender!=owner,"only user is allowed to execute these function");
        require(neoTokens>99 ,"token neo should be greater than 99");
        //check the neo token balance of the user
        token1Balance=token1.balanceOf(msg.sender);
        require(token1Balance >= neoTokens,"neo token balance is not suffiecient ");
        //logic for the conversion of tokens starts
            if((liquiditypool[address(this)].oldAsset1 > 0 && liquiditypool[address(this)].oldAsset2 > 0) && (liquiditypool[address(this)].newAsset1>0 && liquiditypool[address(this)].newAsset2>0)){
                contractBalanceForAsset2=token2.balanceOf(address(this));
                require(contractBalanceForAsset2 >= liquiditypool[address(this)].newAsset2,"contract does not have inr token balance ");
                //transfers the neo tokens from user to these contract 
                token1.transferFrom(msg.sender,address(this),neoTokens);
                temporaryNewAsset1 = liquiditypool[address(this)].newAsset1; 
                temporaryNewAsset2 = liquiditypool[address(this)].newAsset2;
                newAsset1=neoTokens + temporaryNewAsset1;
                priceOf1Token = liquiditypool[address(this)].oldAsset1/newAsset1;
                newAsset2=liquiditypool[address(this)].pool/newAsset1;
                priceOf2Token=liquiditypool[address(this)].oldAsset2/newAsset2;
                totalInrTokens =temporaryNewAsset2 - newAsset2;
                 //transfers the total inr tokens calculated from contract to the user 
                token2.transferFrom(address(this),msg.sender,totalInrTokens);
                temporaryNewAsset1=0;
                temporaryNewAsset2=0;
                //stores  the values of asset 1, asset2 ,price of 1 token and price of 2 token value in the liquidity pool variable after dynamically calculation
                liquiditypool[address(this)].newAsset1 = newAsset1; 
                liquiditypool[address(this)].newAsset2 = newAsset2;
                liquiditypool[address(this)].priceOf1Token=priceOf1Token;
                liquiditypool[address(this)].priceOf2Token=priceOf2Token;
                emit convertNeoToInrtokens("successfully transfer inr tokens to the user and neo tokens to the contract");

            }else if((liquiditypool[address(this)].oldAsset1 >0 && liquiditypool[address(this)].oldAsset2 >0) && liquiditypool[address(this)].newAsset1==0){
                //check for the contract balance for the asset 2 inr token
                contractBalanceForAsset2=token2.balanceOf(address(this));
                require(contractBalanceForAsset2 >= liquiditypool[address(this)].newAsset2,"contract does not have inr token balance ");
                //transfers inr tokens back to the owner from these contract
                token2.transferFrom(address(this),owner,liquiditypool[address(this)].newAsset2);
                liquiditypool[address(this)].oldAsset1=0;
                liquiditypool[address(this)].oldAsset2=0;
                liquiditypool[address(this)].newAsset2=0;
                liquiditypool[address(this)].priceOf1Token=0;
                liquiditypool[address(this)].priceOf2Token=0;
                liquiditypool[address(this)].pool=0;
                emit convertNeoToInrtokens("due to   asset 1 neo tokens balance is 0 ,  asset 2 inr tokens is transfer to the owner and contract owner have to set liquidity again ");
                 
            }else if((liquiditypool[address(this)].oldAsset1 >0 && liquiditypool[address(this)].oldAsset2 >0) && liquiditypool[address(this)].newAsset2==0){
                //check for the contract balance for the asset 1 neo token
                contractBalanceForAsset1=token1.balanceOf(address(this));
                require(contractBalanceForAsset1 >= liquiditypool[address(this)].newAsset1,"contract does not have neo token balance ");
                //transfers neo tokens back to the owner from these contract
                token1.transferFrom(address(this),owner,liquiditypool[address(this)].newAsset1);
                liquiditypool[address(this)].oldAsset1=0;
                liquiditypool[address(this)].oldAsset2=0;
                liquiditypool[address(this)].newAsset1=0;
                liquiditypool[address(this)].priceOf1Token=0;
                liquiditypool[address(this)].priceOf2Token=0;
                liquiditypool[address(this)].pool=0; 
                emit convertNeoToInrtokens("due to  asset 2 inr tokens balance is 0 ,  asset 1 neo tokens is transfer to the owner and contract owner have to set liquidity again ");    
            }else if((liquiditypool[address(this)].oldAsset1 ==0 && liquiditypool[address(this)].oldAsset2 ==0) && (liquiditypool[address(this)].newAsset1==0 && liquiditypool[address(this)].newAsset2==0)){
                emit convertNeoToInrtokens("please add liquidity");        
            }
            

    }

    /*used for converting neo to inr tokens*/
    function convertINRTONEO(uint inrTokens)  public returns(uint){
        require(msg.sender!=owner,"only user is allowed to execute these function");
        require(inrTokens>99 ,"token inr should be greater than 99");
        token2Balance=token2.balanceOf(msg.sender);
        require(token2Balance >= inrTokens,"inrTokens token balance is not enough ");
            if((liquiditypool[address(this)].oldAsset1 > 0 && liquiditypool[address(this)].oldAsset2 > 0) && (liquiditypool[address(this)].newAsset1>0 && liquiditypool[address(this)].newAsset2>0)){
                //check for the contract balance for the asset 1 neo token
                contractBalanceForAsset1=token1.balanceOf(address(this));
                require(contractBalanceForAsset1 >= liquiditypool[address(this)].newAsset1,"contract does not have neo token balance ");
                token2.transferFrom(msg.sender,address(this),inrTokens);
                temporaryNewAsset1 = liquiditypool[address(this)].newAsset1;
                temporaryNewAsset2 = liquiditypool[address(this)].newAsset2;
                newAsset2=inrTokens + temporaryNewAsset2;
                priceOf2Token=liquiditypool[address(this)].oldAsset2/newAsset2;
                newAsset1=liquiditypool[address(this)].pool/newAsset2;
                priceOf1Token = liquiditypool[address(this)].oldAsset1/newAsset1;
                totalNeoTokens =temporaryNewAsset1 - newAsset1;
                //transfers the neo tokens dynamically calculated from contract to the user
                token1.transferFrom(address(this),msg.sender,totalNeoTokens);
                temporaryNewAsset1=0;
                temporaryNewAsset2=0;
                 //stores  the values of asset 1, asset2 ,price of 1 token and price of 2 token value in the liquidity pool variable after dynamically calculation.
                liquiditypool[address(this)].newAsset1 = newAsset1; 
                liquiditypool[address(this)].newAsset2 = newAsset2;
                liquiditypool[address(this)].priceOf1Token=priceOf1Token;
                liquiditypool[address(this)].priceOf2Token=priceOf2Token;
                emit convertInrToNeotokens("successfully transfer neo tokens to the user and inr tokens to the contract");

            }else if((liquiditypool[address(this)].oldAsset1 >0 && liquiditypool[address(this)].oldAsset2 >0) && liquiditypool[address(this)].newAsset1==0){
                //check for the contract balance for the asset 2 inr token
                contractBalanceForAsset2=token2.balanceOf(address(this));
                require(contractBalanceForAsset2 >= liquiditypool[address(this)].newAsset2,"contract does not have inr token balance ");
                //transfers inr tokens back to the owner from these contract
                token2.transferFrom(address(this),owner,liquiditypool[address(this)].newAsset2);
                liquiditypool[address(this)].oldAsset1=0;
                liquiditypool[address(this)].oldAsset2=0;
                liquiditypool[address(this)].newAsset2=0;
                liquiditypool[address(this)].priceOf1Token=0;
                liquiditypool[address(this)].priceOf2Token=0;
                liquiditypool[address(this)].pool=0;
                emit convertInrToNeotokens("due to   asset 1 neo tokens balance is 0 ,  asset 2 inr tokens is transfer to the owner and contract owner have to set liquidity again ");
                 
            }else if((liquiditypool[address(this)].oldAsset1 >0 && liquiditypool[address(this)].oldAsset2 >0) && liquiditypool[address(this)].newAsset2==0){
                //check for the contract balance for the asset 1 neo token
                contractBalanceForAsset1=token1.balanceOf(address(this));
                require(contractBalanceForAsset1 >= liquiditypool[address(this)].newAsset1,"contract does not have neo token balance ");
                //transfers neo tokens back to the owner from these contract
                token1.transferFrom(address(this),owner,liquiditypool[address(this)].newAsset1);
                liquiditypool[address(this)].oldAsset1=0;
                liquiditypool[address(this)].oldAsset2=0;
                liquiditypool[address(this)].newAsset1=0;
                liquiditypool[address(this)].priceOf1Token=0;
                liquiditypool[address(this)].priceOf2Token=0;
                liquiditypool[address(this)].pool=0;    
                emit convertInrToNeotokens("due to  asset 2 inr tokens balance is 0 ,  asset 1 neo tokens is transfer to the owner and contract owner have to set liquidity again");     
            }else if((liquiditypool[address(this)].oldAsset1 ==0 && liquiditypool[address(this)].oldAsset2 ==0) && (liquiditypool[address(this)].newAsset1==0 && liquiditypool[address(this)].newAsset2==0)){
                emit convertInrToNeotokens("please add liquidity");     
            }
            
        
    }


}
